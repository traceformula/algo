/*
This is a simple markov chain approach to generate a new text from an input text (might be from a book!!!)
*/
import java.util.Hashtable;
import java.util.Vector;
import java.util.Random;
import java.io.InputStream;
import java.util.StringTokenizer;
import java.io.StreamTokenizer;
import java.lang.Character;
import java.io.IOException;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class MarkovChain {
    static final int MAXGEN = 10000;
    public static void main(String[] args) throws IOException{
        Chain chain = new Chain();
        int nwords = MAXGEN;
        chain.build(System.in);
        chain.generate(nwords);
    }

}

class Prefix {
    public Vector pref;
    static final int MULTIPLIER = 31;

    public Prefix(Prefix p) {
        pref = (Vector) p.pref.clone();
    }

    public Prefix(int n, String str){
        pref = new Vector();
        //add n elements of str
        for(int i =0; i<n; i++) {
            pref.addElement(str);
        }
    }

    //hashCode for Hashtable to use
    public int hashCode(){
        int h = 0;
        for (int i =0; i<pref.size();i++){
            h = MULTIPLIER * h + pref.elementAt(i).hashCode();
        }
        return h;
    }

    public boolean equals(Object o){
        Prefix p = (Prefix) o;
        for(int i = 0; i<pref.size(); i++) {
            if(! pref.elementAt(i).equals(p.pref.elementAt(i)))
                return false;
        }
        return true;
    }
}

class Chain {
    static final int NPREF = 2;
    static final String NONWORD = "\n";
    Hashtable statetab = new Hashtable(); //key = prefix, value = suffix vector
    Random rand = new Random();
    Prefix prefix = new Prefix(NPREF, NONWORD);

    public void build(InputStream in) throws IOException{
        //Since StreamTokenizer(InputStream) deprecated -> use Reader
        Reader r = new BufferedReader(new InputStreamReader(in));
        StreamTokenizer st = new StreamTokenizer(r);
        st.resetSyntax();
        st.wordChars(0, Character.MAX_VALUE);
        st.whitespaceChars(0, ' ');
        while(st.nextToken()!= st.TT_EOF)
            add(st.sval);
        add(NONWORD);
    }

    public void add(String word){
        Vector suffix = (Vector) statetab.get(prefix);
        if(suffix == null){
            suffix = new Vector();
            statetab.put(new Prefix(prefix), suffix);
        }
        suffix.addElement(word);
        prefix.pref.removeElementAt(0);
        prefix.pref.addElement(word);
    }

    public void generate(int nwords){
        StringBuilder strBuilder = new StringBuilder();
        prefix = new  Prefix(NPREF, NONWORD);
        for(int i =0; i<nwords; i++){
            Vector v = (Vector) statetab.get(prefix);
            int r = Math.abs(rand.nextInt()) % v.size();
            String suffix = (String) v.elementAt(r);
            if(suffix.equals(NONWORD))
                break;
            strBuilder.append(suffix+" ");
            prefix.pref.removeElementAt(0);
            prefix.pref.addElement(suffix);
        }
        System.out.println(strBuilder.toString());
    }
}