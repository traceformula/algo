import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegularExpression {
    
    public static void main(String[] args) {
        String line = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"; //30 characters
        String pattern = "a?a?a?aaa";
        
        long t1 = System.currentTimeMillis();
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(line);
        while(m.find()){
            System.out.println(m.group(0));
         
        }       
        long t2 = System.currentTimeMillis();
        System.out.println(t2-t1);
    }
}