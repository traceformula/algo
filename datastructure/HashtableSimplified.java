/*
This is simplified hashtable, which only takes integers as key.
This hashtable implemented here uses rehashing method to store elements.

In this implementation, it is to demenstrate that a hashtable can be much bigger than the actual
data size.
TODO: Don't use fixed size
*/
public class HashtableSimplified {
    public static final int DEFAULT_SIZE = 5;
    private int dataSize = DEFAULT_SIZE;
    private Data[] data ;
    private int numOfElements = 0;

    public HashtableSimplified(){
        this(DEFAULT_SIZE);
    }
    public HashtableSimplified(int size){

        //check invalid input
        if(size<=0)
            size = DEFAULT_SIZE;
        dataSize = size;

        //lazily intialization
        data = new Data[dataSize];
    }

    public int getSize(){
        return dataSize;
    }
    public int getNumOfElements(){
        return numOfElements;
    }

    public void printData(){
        //better use StringBuilder
        String s = "";
        for(int i = 0; i<data.length; i++){
            if(data[i]==null) 
                s += "[null] ";
            else
                s += "["+data[i].key + "," + data[i].value + "," + data[i].isDeleted + "] ";
        }
        System.out.println(s);
    }

    //hash function
    public int hash(int key){
        return key % dataSize;
    }

    public int rehash(int key){
        return (key + 1) % dataSize;
    }

    //insert a [key,value] pair to the hashtable
    public int add(int key, int value){
        return add(new Data(key, value));
    }

    private int add(Data d){
        if(numOfElements < dataSize){
            int hashId = hash(d.key);
            if(data[hashId] == null){
                numOfElements++;
                data[hashId] = d;                
            }
            else if (data[hashId].key == d.key){
                if(data[hashId].isDeleted)
                    numOfElements++;
                data[hashId] = d;                
            }
            else {
                int count = 0;
                while(count < dataSize){
                    hashId = rehash(hashId);
                    if(data[hashId] == null || data[hashId].key == d.key){
                        if(data[hashId]==null)
                            numOfElements++;
                        else if( data[hashId].isDeleted)
                            numOfElements++;
                        data[hashId] = d;                        
                        break;
                    }
                    if(count == dataSize)
                        return -1;
                    count++;
                }
            }
            return hashId; 
        }
        return -1;
    }

    //Retrieve value by key
    public int get(int key){
        int hashId = hash(key);

        //return -1 if not found
        if(data[hashId] == null) return -1;

        if(!data[hashId].isDeleted && data[hashId].key == key){
            return data[hashId].value;
        }else {
            int count = 0;
            while(count < dataSize){
                hashId = rehash(hashId);

                //return -1 if not found
                if(data[hashId]== null) return -1;

                if(!data[hashId].isDeleted && data[hashId].key == key){
                    return data[hashId].value;
                }

                //return -1 if not found
                if(count==dataSize)
                    return -1;
                count++;
            }
        }
        return -1;
    }

    //Delete 1 element from the hashtable
    public int remove(int key){
        int hashId = hash(key);
        if(data[hashId] == null) return -1;
        if(data[hashId]!=null && !data[hashId].isDeleted &&data[hashId].key == key){
            data[hashId].isDeleted = true;
            numOfElements--;
            return hashId;
        }
        else {
            int count = 0;
            while(count < dataSize){
                if(data[hashId]!= null && !data[hashId].isDeleted && data[hashId].key == key){
                    data[hashId].isDeleted = true;
                    numOfElements--;
                    return hashId;
                }

                if(count == dataSize)
                    return -1;
                count ++;
            }
        }
        return -1;
    }

    class Data {
        int key;
        int value;
        boolean isDeleted = false;
        public Data(int key, int value){
            this.key = key;
            this.value = value;
        }
    }

    public static void main(String[] args){
        HashtableSimplified h =new HashtableSimplified(10);
        System.out.println(h.add(1, 1));
        h.printData();
        System.out.println(h.add(10, 10));
        h.printData();
        System.out.println(h.add(30,30));
        h.printData();
        System.out.println(h.add(32,32));
        h.printData();
        System.out.println(h.add(30,32));
        h.printData();
        System.out.println(h.get(30));
        System.out.println("Number of elements: " + h.getNumOfElements());
        h.remove(10);
        System.out.println("Number of elements: " + h.getNumOfElements());
        System.out.println(h.get(10));
        System.out.println(h.get(30));
        System.out.println(h.get(100));
        System.out.println(h.get(32));
        System.out.println(h.get(1));
        System.out.println(h.getSize());
        h.add(40,40);
        h.add(50,50);
        h.add(60,60);
        h.add(70,70);
        h.add(80,80);
        h.add(90, 90);
        System.out.println("Number of elements: " + h.getNumOfElements());
        h.add(100, 100); //failed to add 100
        System.out.println("Number of elements: " + h.getNumOfElements());
        h.printData();
    }
}
