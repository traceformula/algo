/*
Sort a linked list in O(nlogn) time and constant space complexity.
*/

public class SortList{

    //This method uses recursion
    public ListNode sortList(ListNode head){

        if(head == null) return null;
        if(head.next == null) return head;
        ListNode[] nodes = splitList(head);
        ListNode n1 = nodes[0];
        ListNode n2 = nodes[1];
        n1 = sortList(n1);
        n2 = sortList(n2);
        n1 = mergeList(n1, n2);
        return n1;
    }

    private ListNode[] splitList(ListNode head) {
        
        if(head == null) return new ListNode[]{null, null};
        if(head.next == null) return new ListNode[]{head, null};
        ListNode s = head;
        ListNode f = head;
        
        while(f!=null && f.next != null){
            
            f = f.next.next;
            if(f == null) break;
            s = s.next;
        }
        ListNode middle = s.next;
        s.next = null;
        return new ListNode[]{head, middle};
    }

    private ListNode mergeList(ListNode n1, ListNode n2){

        ListNode source = new ListNode(0);
        ListNode current = source;
        while(true){
            if(n1 == null) {
                current.next = n2;
                break;
            }
            if(n2 == null){
                current.next = n1;
                break;
            }
            if(n1.val < n2.val){
                current.next = n1;
                n1 = n1.next;
            }else {
                current.next = n2;
                n2 = n2.next;
            }
            current = current.next;

        }
        return source.next;
    }

    //non-recusrive way to sort the list using merge sort   
    public ListNode sortListNoRecursion(ListNode head) {
    
        if(head == null || head.next == null) return head;
        int listLength = count(head);
        
        int size = 1;
        int start;
        ListNode n1;
        ListNode n2;
        ListNode current = new ListNode(-1);
        ListNode reserve = current;
        current.next = head;
        while(size<listLength){
            start = 0;
            current = reserve;
            n1 = current.next;
            n2 = nextNodeAt(n1, size);
            while(start+size<listLength){
                mergeSubList(current, n1, n2, size);
                start = start + 2*size;
                current = nextNodeAt(current, size*2);
                if(current == null) break;
                n1 = current.next;
                n2 = nextNodeAt(n1, size);
            }
            size *= 2;

        }
        return reserve.next;
    }

    private int count(ListNode head) {

        if(head == null) return 0;
        int count = 0;
        while(head != null) {
            count++;
            head = head.next;
        }
        return count;
    }

    private ListNode nextNodeAt(ListNode n,int offset){

        if(offset <= 0) return n;
        int count = 0;
        while(count < offset && n!=null){
            n = n.next;
            count ++;
        }
        return n;
    }

    private void mergeSubList(ListNode current, ListNode n1, ListNode n2, int size){

        //don't care about whether size <= 0
        if(n1==null||n2==null) return;
        int count1 = 0;
        int count2 = 0;
        
        ListNode reserve = current;
        while(count1<size && count2<size){
            if(n1 == null) {
                current.next = n2;
                count1 = size;
                break;
            }
            if(n2 == null){
                current.next = n1;
                count2 = size;
                break;
            }
            if(n1.val < n2.val){
                current.next = n1;
                n1 = n1.next;
                count1++;
            }
            else{
                current.next = n2;
                n2 = n2.next;
                count2++;
            }
            current = current.next;
        }

        if(count1 < size && count2 == size){
            current.next = n1;
            while(count1<size && n1!=null){
                count1++;                           
                if(count1==size)
                    n1.next = n2;
                else n1 = n1.next; 
            }
        }
        else if(count1 == size && count2 < size){
            current.next = n2;
            while(count2<size && n2!=null){
                n2 = n2.next;                
                count2++;
            }
        }
    }

    public static void main(String [] args){

        SortList s = new SortList();
        ListNode n1 = generateListNode();
        //print(s.sortList(n1));
        //n1 = generateListNode();
        print(s.sortListNoRecursion(n1));
    }

    private static ListNode generateListNode(){
        
        ListNode n1 = new ListNode(4);
        ListNode n2 = new ListNode(3);
        ListNode n3 = new ListNode(5);
        ListNode n4 = new ListNode(6);
        ListNode n5 = new ListNode(0);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;

        return n1;
    }

    private static void print(ListNode n){

        if(n==null){
            System.out.println("null");
            return;
        }
        StringBuilder sb = new StringBuilder();
        while(n!=null){
            sb.append(n.val + " ");
            n = n.next;
        }
        System.out.println(sb.toString());
    }
}

class ListNode {
    int val;
    ListNode next;
    public ListNode(int x){
        val = x;
        next = null;
    }
}