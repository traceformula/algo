/*
Problem description:
Given a triangle, find the minimum path sum from top to bottom. Each step you may move to adjacent numbers on the row below.

For example, given the following triangle
[
     [2],
    [3,4],
   [6,5,7],
  [4,1,8,3]
]
The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).

Note:
Bonus point if you are able to do this using only O(n) extra space, where n is the total number of rows in the triangle.
*/

import java.util.List;
import java.util.ArrayList;

public class Triangle {

    public int minimumTotal(List<List<Integer>> triangle){
        if(triangle.size() == 0) return 0;
        if(triangle.size() == 1) return triangle.get(0).get(0);
        int size = triangle.size();
        int [] ar = new int[size];

        //Initialize the array to the last row
        List<Integer> l = triangle.get(size-1);
        for(int i=0; i<size; i++){
            ar[i] = l.get(i);
        }

        for(int i=size-2; i>=0; i--){
            l = triangle.get(i);

            for(int j=0;j<=i; j++){
                if(ar[j]<ar[j+1])
                    ar[j] = l.get(j) + ar[j];
                else
                    ar[j] = l.get(j) + ar[j+1];
            }
        }
        return ar[0];
    }
    public static void main(String[] args){
        List<List<Integer>> triangle = new ArrayList<List<Integer>>();
        List<Integer> t1 = new ArrayList<Integer>();
        t1.add(2);
        triangle.add(t1);
        List<Integer> t2 = new ArrayList<Integer>();
        t2.add(3); t2.add(4);
        triangle.add(t2);
        List<Integer> t3 = new ArrayList<Integer>();
        t3.add(6); t3.add(5); t3.add(7);
        triangle.add(t3);
        List<Integer> t4 = new ArrayList<Integer>();
        t4.add(4); t4.add(1); t4.add(8); t4.add(3);
        triangle.add(t4);
        System.out.println( new Triangle().minimumTotal(triangle));
    }
}