/*
Problem Description:
Given a binary tree, find its minimum depth.

The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.
*/

public class BiTreeMinimumDepth {

    public int minDepth(TreeNode root){
        return minDepthRecursive(root);
    }

    //A solution using recursion
    private int minDepthRecursive(TreeNode root){
        if(root == null) return 0;
        if(root.left == null) return 1+ minDepthRecursive(root.right);
        if(root.right == null) return 1+ minDepthRecursive(root.left);

        int a = minDepthRecursive(root.left);
        int b = minDepthRecursive(root.right);
        return 1 + (a>b?b:a);
    }

    //Non-recursive using 
    private int minDepthNonRecursive(TreeNode root){
        //https://oj.leetcode.com/discuss/6285/solve-it-using-union-find
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;
    TreeNode(int x) { val = x; }
}