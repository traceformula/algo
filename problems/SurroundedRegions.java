/*
Problem Description:
Given a 2D board containing 'X' and 'O', capture all regions surrounded by 'X'.

A region is captured by flipping all 'O's into 'X's in that surrounded region.

For example,
X X X X
X O O X
X X O X
X O X X
After running your function, the board should be:

X X X X
X X X X
X X X X
X O X X
*/

import java.util.Queue;
import java.util.LinkedList;

public class SurroundedRegions {

    public void solve(char[][] board){
        int rn = board.length;//number of rows
        if(rn == 0) return;
        int cn = board[0].length; // number of columns;

        for(int i=0; i<rn; i++){
            search(board, i, 0);
            search(board, i, cn-1);
        }

        for(int i=0; i<cn; i++){
            search(board, 0, i);
            search(board, rn-1, i);
        }

        for(int i=0; i<rn; i++){
            for (int j=0; j<cn; j++){
                if(board[i][j]=='o')
                    board[i][j] = 'x';
                else if(board[i][j] == 'a')
                    board[i][j] = 'o';
            }
        }
    }

    private void search(char[][] board, int x, int y){
        Queue<Integer> q = new LinkedList<Integer>();
        c(board, x, y, q);
        int cn = board[0].length;
        while(!q.isEmpty()){
            int i = q.remove();
            int cl = i % cn;
            int r = i/cn;
            c(board, r+1, cl, q);
            c(board, r-1, cl, q);
            c(board, r, cl+1, q);
            c(board, r, cl-1, q);
        }
    }

    private void c(char[][] board, int x, int y, Queue<Integer> q){
        int cn = board[0].length;
        if(x>=0 && x<board.length && y>=0 && y<cn){
            if(board[x][y] == 'o'){
                board[x][y] = 'a';
                q.add(x*cn + y);
            }
        }
    }

    public static void main(String[] args){
        SurroundedRegions s = new SurroundedRegions();
        //char[][] board = new char[][]{{'x','x','x'},{'x','o','x'},{'x','x','x'}};
        char[][] board = new char[][]{
            {'x', 'o', 'x', 'o','x','o'},
            {'o', 'x', 'o', 'x','o','x'},
            {'x', 'o', 'x', 'o','x','o'},
            {'o', 'x', 'o', 'x','o','x'}
        };
        printBoard(board);
        s.solve(board);
        printBoard(board);
    }

    private static void printBoard(char[][] board){
        for(int i=0; i<board.length; i++){
            for(int j=0; j<board[0].length; j++){
                System.out.print(board[i][j]);
            }
            System.out.println("");
        }
    }
}
