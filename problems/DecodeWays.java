/*
Problem description:
A message containing letters from A-Z is being encoded to numbers using the following mapping:

'A' -> 1
'B' -> 2
...
'Z' -> 26
Given an encoded message containing digits, determine the total number of ways to decode it.

For example,
Given encoded message "12", it could be decoded as "AB" (1 2) or "L" (12).

The number of ways decoding "12" is 2.
*/

public class DecodeWays {

    public int numDecodings(String s){

        if(s == null) return 0; //throws exception ?
        if(s.length() == 0) return 0;
        if(s.length() == 1) {
            if(s.charAt(0)=='0') return 0;
            else return 1;
        }
        int a;
        int b; 
        int l = s.length();
        int temp;
        char c;
        char c1;

        b = (s.charAt(l-1)=='0')?0:1;
        a = (s.charAt(l-2)=='1'||(s.charAt(l-2)=='2'&&s.charAt(l-1)<'7'))?
            (1+b):(s.charAt(l-2)=='0'?0:b);

        if(l==2) return a;

        for(int i = l-3;i>-1; i--){
            c = s.charAt(i);
            c1 = s.charAt(i+1);
            if(c=='0'){
                b = a;
                a = 0;
            }
            else if(c=='1'||(c=='2'&&c1<'7')){
                if(c1=='0'){
                    a = b;
                    b = 0;
                }else{
                    temp = a;
                    a = a + b;
                    b = temp;
                }
            }
            else {  
                if(c1=='0')
                    return 0;              
                b = a;
            }
        }

        return a;
    }

    public static void main(String [] args){

        DecodeWays d = new DecodeWays();
        System.out.println(d.numDecodings("26341220"));
        System.out.println(d.numDecodings("12"));
        System.out.println(d.numDecodings("100"));
        System.out.println(d.numDecodings("0"));
        System.out.println(d.numDecodings("27"));
        System.out.println(d.numDecodings("30"));
        System.out.println(d.numDecodings("01"));
    }
}