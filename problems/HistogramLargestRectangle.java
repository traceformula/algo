/*
Problem Description:
Given n non-negative integers representing the histogram's bar height where the width of each bar is 1, 
find the area of largest rectangle in the histogram.
         4
      3  __
 2    __|__|
__  1|//|//| 1
  |__|//|//|__
__|__|__|__|__|

Above is a histogram where width of each bar is 1, given height = [2,1,3,4,1].
The largest rectangle is shown in the shaded area, which has area = 6 units.

*/

import java.util.Stack;

public class HistogramLargestRectangle {

    public int largestRectangleArea(int[] height){
        //return simpleLargestRectangleArea(height);
        return fastLargestRectangleArea(height);
    }
    
    private int fastLargestRectangleArea(int[] height){
        if(height == null || height.length == 0) return 0;

        Stack<Integer> s = new Stack<Integer>();
        s.push(-1);
        int max = 0;

        for(int i=0; i<height.length; i++) {
            
            while(s.peek()!=-1){
                if(height[s.peek()]>height[i]){
                    int top = s.pop();
                    max = Math.max(max, height[top]*(i-1-s.peek()));
                }
                else
                    break;
            }            
            s.push(i);            
        }

        while(s.peek()!=-1){
            int top = s.pop();
            max = Math.max(max, height[top]*(height.length-1-s.peek()));
        }
        return max;
    }   

    //This method is too slow
    private int simpleLargestRectangleArea(int[] height){

        if(height.length == 0) return 0;
        if(height.length == 1) return height[0];
        int max = 0;
        int length = height.length;
        int tempArea;
        int a;

        for(int i=0; i<length; i++){
            
            a = height[i];
            tempArea = a;
            for(int j=i+1; j<length; j++){
                if(height[j]>=a)
                    tempArea += a;
                else
                    break;
            }
            if(i>0){
                for(int j=i-1; j>=0; j--){
                    if(height[j]>=a)
                        tempArea += a;
                    else
                        break;
                }
            }

            if(tempArea > max)
                max = tempArea;
        }

        return max;
    }

    public static void main(String[] args){

        int[] height = new int[]{2,1,3,4,1,1,1};
        System.out.println((new HistogramLargestRectangle()).largestRectangleArea(height));
    }
}


