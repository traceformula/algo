/*
Problem Description:
Given a string containing only digits, restore it by returning all possible valid IP address combinations.

For example:
Given "25525511135",

return ["255.255.11.135", "255.255.111.35"]. (Order does not matter)
*/

import java.util.List;
import java.util.ArrayList;

public class RestoreIpAddresses {

    public List<String> restoreIpAddresses(String s){

        List<String> results = new ArrayList<String>();

        int l = s.length();
        if(s==null || l<3 || l>12) return results;

        int[] dp = new int[]{0,1,2,3,l}; //position of the dots
        int index = 0;

        while(true){
            if(isValid(s, dp)){
                results.add(restore(s, dp));                
            }
            if(!generateNext(dp, l))
                break;
        }

        return results;
    }

    private boolean isValid(String s, int[] dots){

        for(int i=1; i<dots.length; i++){
            if(s.charAt(dots[i])=='0'
            if(Integer.parseInt(s.substring(dots[i-1],dots[i]))>255) return false;
        }
        return true;
    }

    private String restore(String s, int[] dots){

        StringBuilder builder = new StringBuilder();
        for(int i=1; i<dots.length-1; i++){
            builder.append(s.substring(dots[i-1], dots[i])+".");
        }
        builder.append(s.substring(dots[dots.length-2], dots[dots.length-1]));
        return builder.toString();
    }

    public boolean generateNext(int[] dots, int length){

        for(int i=dots.length-2; i>0;i--){
            if(dots[i]+1==dots[i+1]){
                if(i==1) return false;
                else
                    continue;
            }                
            dots[i] += 1;
            if(dots[i]>dots[i-1]+3)
                continue;
            else{
                for(int j=i+1;j<dots.length-1;j++){
                    dots[j] = dots[j-1]+1;
                }
                break;
            }                
        }
        if(dots[1]-dots[0]>3) return false;
        return true;
    }

    public static void main(String[] args){

        print("25525511135");
        print("0000");
    }

    private static void print(String s){
        RestoreIpAddresses r = new RestoreIpAddresses();
        List<String> ss = r.restoreIpAddresses(s);
        for(String sss: ss){
            System.out.println(sss);
        }
    }
}