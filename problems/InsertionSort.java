/*
Problem description:
Sort a linked list using insertion sort
*/
import List
public class InsertionSort {

    public ListNode insertionSortList(ListNode head) {
        if(head == null || head.next == null) return head;
        
        ListNode current = head.next;
        ListNode a; //to store 1 element picked
        ListNode b;
        

        //head is always sorted linked list
        head.next = null;

        while(current!=null){
            a = current;
            current = current.next;
            a.next = null;
            b = head;
            if(a.val <= b.val){
                a.next = head;
                head = a;
                continue;
            }
            while(true){
                if(b.next == null){
                   b.next = a;
                   break;
                }else {
                    if(b.next.val < a.val){
                        b = b.next;
                    }else{
                        a.next = b.next;
                        b.next = a;
                        break;
                    }
                }
            }
        }

        return head;
    }

    public static void main(String[] args){
        ListNode n = generateListNode();
        print((new InsertionSort()).insertionSortList(n));
    }

    private static ListNode generateListNode(){

        ListNode n1 = new ListNode(4);
        ListNode n2 = new ListNode(3);
        ListNode n3 = new ListNode(5);
        ListNode n4 = new ListNode(6);
        ListNode n5 = new ListNode(0);
        n1.next = n2;
        n2.next = n3;
        n3.next = n4;
        n4.next = n5;

        return n1;
    }

    private static void print(ListNode n){

        if(n==null){
            System.out.println("null");
            return;
        }
        StringBuilder sb = new StringBuilder();
        while(n!=null){
            sb.append(n.val + " ");
            n = n.next;
        }
        System.out.println(sb.toString());
    }
}

class ListNode {
    int val;
    ListNode next;
    public ListNode(int x){
        val = x;
    }
}


