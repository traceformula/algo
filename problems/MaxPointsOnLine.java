/*
Problem description:
Given n points on a 2D plane, find the maximum number of points that lie on the same straight line.

TODO: for float/double coordinated points.
*/
import java.util.Map;
import java.util.HashMap;

public class MaxPointsOnLine {

    public int maxPoints(Point[] points){
        if(points == null || points.length == 0)
            return 0;
        if(points.length == 1)
            return 1;
        int maxPoints = 0;

        Map<Integer,Map<Integer, Integer>> map = new HashMap<Integer, Map<Integer, Integer>>();
        for (int i=0; i<points.length-1; i++){
            map.clear();
            int sameValues = 0;
            Point p = points[i];
            int max=0;
            for(int j=i+1; j<points.length;j++){
                Point q = points[j];

                if(p.x==q.x && p.y==q.y){
                    sameValues++;
                    continue;
                }
                
                int x = p.x - q.x;
                int y = p.y - q.y;
                int g = gcd(x,y);
                //don't care about the case where g = 0;
                x = x/g;
                y = y/g;
                if(!map.containsKey(x)){
                    Map<Integer, Integer> m = new HashMap<Integer, Integer>();
                    m.put(y, 1);
                    map.put(x,m);
                }
                else {
                    if(map.get(x).containsKey(y)){
                        map.get(x).put(y, map.get(x).get(y)+1);
                    }else{
                        map.get(x).put(y, 1);
                    }
                }                    
                
                max = Math.max(max, map.get(x).get(y));
            }
            maxPoints = Math.max(max+1+sameValues, maxPoints);
        }

        return maxPoints;
    }

    private int  gcd(int a, int b){
        //don't count for the case that a == b == 0;
        if(b==0) return a;
        if(a==0) return b; //unncessary checking
        return gcd(b, a%b);
    }

    public static void main (String[] args){
        MaxPointsOnLine m = new MaxPointsOnLine();
        /*System.out.println(m.gcd(0,9));
        System.out.println(m.gcd(9,0));
        System.out.println(m.gcd(9,3));
        System.out.println(m.gcd(3,9));
        System.out.println(m.gcd(3,8));*/

        Point p1 = new Point(0,0);
        Point p2 = new Point(0,1);
        Point p3 = new Point(0,3);
        Point p4 = new Point(1,0);
        Point p5 = new Point(1,0);
        Point p6 = new Point(1,0);
        Point p7 = new Point(1,0);
        Point[] points = new Point[]{p1, p2, p3, p4, p5, p6, p7};
        System.out.println(m.maxPoints(points));
    }
}

class Point {
    int x;
    int y;

    Point() { x=0; y=0;}
    Point(int a, int b){
        x = a;
        y = b;
    }
}