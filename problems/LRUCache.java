/*
Problem Description:
Design and implement a data structure for Least Recently Used (LRU) cache. 
It should support the following operations: get and set.

get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
set(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity, 
it should invalidate the least recently used item before inserting a new item.
*/
import java.util.Map;
import java.util.HashMap;

public class LRUCache {

    Map<Integer, Node> map; //
    int capacity;
    Node head = null;
    Node tail = null;

    public LRUCache(int capacity){
       this.capacity = capacity;
       map = new HashMap<Integer, Node>();
    }

    public int get(int key){
        Integer k = new Integer(key);
        if(map.containsKey(k)){
            Node n = (Node) map.get(k);
            move(n);
            return n.value;
        }
        return -1;
    }

    public void set(int key, int value){
        Integer k = new Integer(key);
        if(map.containsKey(k)){
            Node n = (Node) map.get(k);
            n.value = value;
            move(n);
        }else{
            Node n = new Node(null, null, key, value);
            if(capacity <= 0) return;
            if(capacity > map.size()){                
                if(tail == null){
                    head = tail = n;
                }else {
                    tail.next = n;
                    n.prev = tail;
                    tail = n;
                }
                map.put(new Integer(key), n);
            }
            else {
                Node next = head.next;

                //case where capacity == 1
                if(next==null){
                    map.remove(new Integer(head.key));
                    map.put(new Integer(n.key),n);
                    head = tail = n;
                }
                else {
                    map.remove(new Integer(head.key)); 
                    map.put(new Integer(n.key),n);                   
                    tail.next = n;
                    n.prev = tail;
                    head = head.next;
                    tail = n;
                }
            }
        }
    }

    //move the element to the end when get() is called
    private void move(Node node){
        //assume that if move is called, there is at least 1 element
        if(node.key == tail.key)
            return;
        if(node.key == head.key)
            head = node.next;
        Node prev = node.prev;
        Node next = node.next;

        if(prev!=null)
            prev.next = next;
        if(next!=null)
            next.prev = prev;

        node.prev = tail;
        node.next = null;
        tail.next = node;
        tail = node;
    }

    //Double-linked list
    class Node {
        Node prev;
        Node next;
        int key;
        int value;
        public Node(Node prev, Node next, int key, int value){
            this.prev = prev;
            this.next = next;
            this.key = key;
            this.value = value;
        }
    }
}