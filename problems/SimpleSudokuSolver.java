/*
This is a simple sudoku solver.
For more complicated & efficient solution, see Dancing Links X Algorithm
introduced by Knuth at
http://www.ocf.berkeley.edu/~jchu/publicportal/sudoku/sudoku.paper.html

In this implementation, it is assumed that the sudoku board is of size of 9x9
*/
public class SimpleSudokuSolver {

    //board[i][j] == '.' indicates that the cell is blank
    public void solveSudoku(char[][] board){

        if(board == null || board.length ==0)
            return;
        solve(board);
    }

    public boolean solve(char[][] board){

        for(int i = 0; i<board.length; i++){
            for (int j = 0; j<board[0].length; j++){
                if(board[i][j]=='.'){
                    for(char c='1'; c<'9'; c++){
                        if(isValid(board, i, j, c)){
                            board[i][j] = c;
                            if (solve(board))
                                return true;
                            else{
                                board[i][j] = '.';
                            }
                        }
                    }
                    return false;
                }
            }
        }
        return true;
    }

    private boolean isValid(char[][] board, int i, int j, char c){

        for(int k=0; k<board.length; k++){
            if(board[k][j] == c)
                return false;
        }

        for(int k=0; k<board[0].length; k++){
            if(board[i][k] == c)
                return false;
        }

        for(int k1=(i/3*3);k1<(i/3*3+3);k1++)
            for(int k2=(j/3*3);k2<(j/3*3+3);k2++){
                if (board[k1][k2] == c) 
                    return false;
            }

        return true;
    }
}