/*
Problem description:
Nodes are labeled uniquely.

We use # as a separator for each node, and , as a separator for node label and each neighbor of the node.
As an example, consider the serialized graph {0,1,2#1,2#2,2}.

The graph has a total of three nodes, and therefore contains three parts as separated by #.

First node is labeled as 0. Connect node 0 to both nodes 1 and 2.
Second node is labeled as 1. Connect node 1 to node 2.
Third node is labeled as 2. Connect node 2 to node 2 (itself), thus forming a self-cycle.
*/
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;

import java.util.Queue;
import java.util.LinkedList;

public class CloneGraph {

    public UndirectedGraphNode cloneGraph(UndirectedGraphNode node){
        if (node==null)
            return null;
        Map<UndirectedGraphNode, UndirectedGraphNode> map = new HashMap<UndirectedGraphNode, UndirectedGraphNode>();
        Queue<UndirectedGraphNode> queue = new LinkedList<UndirectedGraphNode>();
        
        UndirectedGraphNode n = new UndirectedGraphNode(node.label);
        map.put(node, n);
        queue.add(node);

        while(!queue.isEmpty()){
            UndirectedGraphNode tempNode = queue.remove(); 
            UndirectedGraphNode t = map.get(tempNode);   

            for(UndirectedGraphNode neighbor: tempNode.neighbors){                
                if(!map.containsKey(neighbor)){
                    UndirectedGraphNode neighborNode = new UndirectedGraphNode(neighbor.label);
                    map.put(neighbor, neighborNode);
                    queue.add(neighbor);
                    t.neighbors.add(neighborNode);
                }
                else {
                    t.neighbors.add(map.get(neighbor));
                }
            }
         
        }
        return n;
    }

    public static void main(String[] args){
        UndirectedGraphNode n1 = new UndirectedGraphNode(1);
        UndirectedGraphNode n2 = new UndirectedGraphNode(2);
        UndirectedGraphNode n3 = new UndirectedGraphNode(3);
        n1.neighbors.add(n2);
        n1.neighbors.add(n3);
        n2.neighbors.add(n1);
        n2.neighbors.add(n3);
        n3.neighbors.add(n1);
        n3.neighbors.add(n2);
        printGraph(n1, n2, n3);

        UndirectedGraphNode n = new CloneGraph().cloneGraph(n1);
        printGraph(n);
    }

    private static void printGraph(UndirectedGraphNode... nodes){
        for(UndirectedGraphNode n: nodes){
            String s = "" + n.label + ":";
            for(UndirectedGraphNode nn: n.neighbors){
                s += nn.label + " ";
            }
            System.out.println(s);
        }
    }
}

class UndirectedGraphNode {
    int label;
    List<UndirectedGraphNode> neighbors;
    public UndirectedGraphNode(int x){
        label = x;
        neighbors = new ArrayList<UndirectedGraphNode>();
    }
}
