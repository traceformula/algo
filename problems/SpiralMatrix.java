/*
Problem Description:
Given a matrix of m x n elements (m rows, n columns), return all elements of the matrix in spiral order.

For example,
Given the following matrix:

[
 [ 1, 2, 3 ],
 [ 4, 5, 6 ],
 [ 7, 8, 9 ]
]
You should return [1,2,3,6,9,8,7,4,5].
*/

import java.util.List;
import java.util.ArrayList;

public class SpiralMatrix {

    public List<Integer> spiralOrder(int[][] matrix){

        List<Integer> l = new ArrayList<Integer>();
        if(matrix.length == 0 || matrix[0].length == 0) return l;

        int maxX = matrix[0].length-1;
        int minX = 0;
        int maxY = matrix.length-1;
        int minY = 0;

        int x = 0; int y=0;

        //use -1, 0, 1 for direction
        int xDirection = 1;
        int yDirection = 0;

        l.add(matrix[y][x]);

        while(true){
            if(minX>maxX || minY>maxY) break;
            if(xDirection==1){
                if(x>=maxX){
                    xDirection = 0;
                    yDirection = 1;
                    minY++;
                    continue;
                }
            }else if(xDirection==-1){
                if(x<=minX){
                    xDirection = 0;
                    yDirection = -1;
                    maxY--;
                    continue;
                }
            }else if(yDirection == 1){
                if(y>=maxY){
                    xDirection = -1;
                    yDirection = 0;
                    maxX--;
                    continue;
                }
            }else if(yDirection == -1){
                if(y<=minY){
                    xDirection = 1;
                    yDirection = 0;
                    minX++;
                    continue;
                }
            }
            x += xDirection;
            y += yDirection;

            l.add(matrix[y][x]);
        }
        return l;
    }
}
    public static void main(String [] args){

        int[][] matrix = new int[][]{
                     { 1, 2, 3 },
                     { 4, 5, 6 },
                     { 7, 8, 9 }
                    };
        SpiralMatrix s = new SpiralMatrix();
        print(s.spiralOrder(matrix));

        matrix = new int[][]{{2,3}};
        print(s.spiralOrder(matrix));
    }

    private static void print(List<Integer> list){

        for(int i=0;i<list.size();i++)
            System.out.print(list.get(i));
    }
}